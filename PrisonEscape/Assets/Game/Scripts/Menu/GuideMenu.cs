﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuideMenu : MonoBehaviour
{
    #region Singleton;

    public static GuideMenu instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public GameObject panel, background;
    public GameObject text, text1, text2, text3, text4, text5, text6, text7;
    public int page;
    public bool inGuide;

    void Start()
    {
        panel.SetActive(false);
        background.SetActive(false);
        text.SetActive(false);
        text1.SetActive(false);
        text2.SetActive(false);
        text3.SetActive(false);
        text4.SetActive(false);
        text5.SetActive(false);
        text6.SetActive(false);
        text7.SetActive(false);
    }
    void Update()
    {
        ActiveGuide();
        ChangePage();
    }
    private void ChangePage()
    {
        if (page == 0)
        {
            text.SetActive(true);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 1)
        {
            text.SetActive(false);
            text1.SetActive(true);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 2)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(true);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 3)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(true);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 4)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(true);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 5)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(true);
            text6.SetActive(false);
            text7.SetActive(false);
        }
        else if (page == 6)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(true);
            text7.SetActive(false);
        }
        else if (page == 7)
        {
            text.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
            text3.SetActive(false);
            text4.SetActive(false);
            text5.SetActive(false);
            text6.SetActive(false);
            text7.SetActive(true);
        }
        if (page > 8)
        {
            page = 0;
        }
        if (page < 0)
        {
            page = 8;
        }
    }
    private void ActiveGuide()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            inGuide = true;

            if (inGuide)
            {
                MenuInGame.instance.PauseGame();
                MenuInGame.instance.crossaim.SetActive(false);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                panel.SetActive(true);
                background.SetActive(true);
                text.SetActive(true);
            }
        }
        else if (inGuide == false && MenuInGame.instance.pauseActive == false && MenuInGame.instance.lost == false && MenuInGame.instance.win == false)
        {
            Time.timeScale = 1f;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            MenuInGame.instance.crossaim.SetActive(true);
            panel.SetActive(false);
            background.SetActive(false);
            text.SetActive(false);
        }
    }
    public void CloseButton()
    {
        inGuide = false;
        MenuInGame.instance.pauseActive = false;
        MenuInGame.instance.crossaim.SetActive(true);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        panel.SetActive(false);
        background.SetActive(false);
        text.SetActive(false);
        text1.SetActive(false);
        text2.SetActive(false);
        text3.SetActive(false);
        text4.SetActive(false);
        text5.SetActive(false);
        text6.SetActive(false);
        text7.SetActive(false);
    }
    public void NextButton()
    {
        page += 1;
    }
    public void BackButton()
    {
        page -= 1;
    }
}
