﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitiesHUD : MonoBehaviour
{
    #region Singleton;

    public static AbilitiesHUD instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public GameObject image, image1, image2, image3, hidden;
    public Text cDFA, cDSA, cDTA;
    public float time;
    void Start()
    {
        image.SetActive(false);
        image1.SetActive(false);
        image2.SetActive(false);
        image3.SetActive(false);
        hidden.SetActive(false);
        cDFA.gameObject.SetActive(false);
        cDSA.gameObject.SetActive(false);
        cDTA.gameObject.SetActive(false);
    }
}
