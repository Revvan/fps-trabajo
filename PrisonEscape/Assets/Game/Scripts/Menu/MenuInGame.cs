﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class MenuInGame : MonoBehaviour
{
    #region Singleton;

    public static MenuInGame instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public GameObject Panel;
    public GameObject BackGround;
    public GameObject continueB;
    public GameObject restartB;
    //public GameObject mainMenuB;
    public GameObject exitB;
    public GameObject pauseT;
    public GameObject crossaim;

    public GameObject newPanel;
    public Text scoreText, winT, lostT;
    public bool lost, win;

    //restartB, newExitB, newBackM,lostT

    public bool pauseActive;

    public AudioSource music;

    void Start()
    {
        music.Play();

        Time.timeScale = 1;
        Panel.SetActive(false);
        BackGround.SetActive(false);
        pauseT.SetActive(false);
        continueB.SetActive(false);
        restartB.SetActive(false);
        //mainMenuB.SetActive(false);
        exitB.SetActive(false);
        crossaim.SetActive(true);

        newPanel.SetActive(false);
        scoreText.gameObject.SetActive(false);
        winT.gameObject.SetActive(false);
        lostT.gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
            GuideMenu.instance.inGuide = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            music.Pause();

            Panel.SetActive(true);
            BackGround.SetActive(true);
            pauseT.SetActive(true);
            continueB.SetActive(true);
            restartB.SetActive(true);
            //mainMenuB.SetActive(true);
            exitB.SetActive(true);
            crossaim.SetActive(false);
        }
        else if (pauseActive == false && GuideMenu.instance.inGuide == false && lost == false && win == false)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            music.UnPause();

            Panel.SetActive(false);
            BackGround.SetActive(false);
            pauseT.SetActive(false);
            continueB.SetActive(false);
            restartB.SetActive(false);
            //mainMenuB.SetActive(false);
            exitB.SetActive(false);
            crossaim.SetActive(true);
        }

        Lost();
        Win();
    }
    private void Lost()
    {
        if (lost)
        {
            PauseGame();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            music.Stop();
            newPanel.SetActive(true);
            winT.gameObject.SetActive(false);
            lostT.gameObject.SetActive(true);
            exitB.SetActive(true);
            //mainMenuB.SetActive(true);
            //scoreText.gameObject.SetActive(true);
            crossaim.SetActive(false);

            //scoreText.text = "Your score: " + ScoreScript.instance.actualScore.ToString();
        }
    }
    private void Win()
    {
        if (win)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            music.Stop();
            newPanel.SetActive(true);
            winT.gameObject.SetActive(true);
            lostT.gameObject.SetActive(false);
            exitB.SetActive(true);
            //mainMenuB.SetActive(true);
            //scoreText.gameObject.SetActive(true);
            crossaim.SetActive(false);

            winT.text = "You escape of the prison!";
            //scoreText.text = "Your score: " + ScoreScript.instance.actualScore.ToString();
        }
    }
    public void PauseGame()
    {
        if (Time.timeScale == 1)
        {
            pauseActive = true;
            Time.timeScale = 0;
        }
        else
        {
            pauseActive = false;
            Time.timeScale = 1;
        }
    }
    public void buttonContinue()
    {
        pauseActive = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Panel.SetActive(false);
        BackGround.SetActive(false);
        pauseT.SetActive(false);
        continueB.SetActive(false);
        //mainMenuB.SetActive(false);
        exitB.SetActive(false);
        crossaim.SetActive(true);
    }
    public void buttonRestart()
    {
        SceneManager.LoadScene(0);
    }
    //public void buttonBackToMainMenu()
    //{
    //    SceneManager.LoadScene(1);
    //}
    public void CloseGame()
    {
        Application.Quit();
    }
}
