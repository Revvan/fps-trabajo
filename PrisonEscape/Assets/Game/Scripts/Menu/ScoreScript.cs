﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    #region Singleton

    public static ScoreScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public Text score;
    public int actualScore, scoreValue, scoreValueForKillingBoss;
    void Start()
    {
        //score.gameObject.SetActive(true);
        score.text = "Score: " + actualScore.ToString();
    } 

    void Update()
    {
        score.text = "Score: " + actualScore.ToString();
    }
}
