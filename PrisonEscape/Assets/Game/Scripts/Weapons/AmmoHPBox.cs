﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoHPBox : MonoBehaviour
{
    #region Singleton;

    public static AmmoHPBox instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public int hpGain;
    public int ammoPistolGain;
    public int ammoShotgunGain;
    public int ammoRifleGain;

    public AudioSource audioSource;
    public AudioClip ammoClip;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            audioSource.PlayOneShot(ammoClip);

            if (PlayerControllerScript.instance.currentHP >= PlayerControllerScript.instance.maxHP)
            {
                PlayerControllerScript.instance.currentHP += 0;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (PlayerControllerScript.instance.currentHP > 75)
            {
                int h = PlayerControllerScript.instance.maxHP - PlayerControllerScript.instance.currentHP;
                PlayerControllerScript.instance.currentHP += h;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (PlayerControllerScript.instance.currentHP <= 75)
            {
                PlayerControllerScript.instance.currentHP += hpGain;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }

            //Reload Pistol Ammo
            if (PistolScript.instance.actualMaxAmmo >= PistolScript.instance.maxAmmo)
            {
                PistolScript.instance.actualMaxAmmo += 0;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (PistolScript.instance.actualMaxAmmo >= 66)
            {
                int a = PistolScript.instance.maxAmmo - PistolScript.instance.actualMaxAmmo;
                PistolScript.instance.actualMaxAmmo += a;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (PistolScript.instance.actualMaxAmmo <= 65)
            {
                PistolScript.instance.actualMaxAmmo += ammoPistolGain;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }

            //Reload Shotgun Ammo
            if (ShotGunScript.instance.actualMaxAmmo >= ShotGunScript.instance.maxAmmo)
            {
                ShotGunScript.instance.actualMaxAmmo += 0;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (ShotGunScript.instance.actualMaxAmmo <= 70)
            {
                ShotGunScript.instance.actualMaxAmmo += ammoShotgunGain;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }

            //Reload MachineGun Ammo
            if (MachineGunScript.instance.actualMaxAmmo >= MachineGunScript.instance.maxAmmo)
            {
                MachineGunScript.instance.actualMaxAmmo += 0;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (MachineGunScript.instance.actualMaxAmmo >= 101)
            {
                int c = MachineGunScript.instance.maxAmmo - MachineGunScript.instance.actualMaxAmmo;
                MachineGunScript.instance.actualMaxAmmo += c;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
            if (MachineGunScript.instance.actualMaxAmmo <= 100)
            {
                MachineGunScript.instance.actualMaxAmmo += ammoRifleGain;
                audioSource.PlayOneShot(ammoClip);
                Destroy(this.gameObject);
            }
        }
    }
}
