﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolMachineGunBullet : MonoBehaviour
{
    #region Singleton

    public static PistolMachineGunBullet instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damagePistolBullet, damageMachineGunBullet;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject, 0f);
        }
    }
}
