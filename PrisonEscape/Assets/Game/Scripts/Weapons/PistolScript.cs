﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolScript : MonoBehaviour
{
    #region Singleton;

    public static PistolScript instance;

    #endregion;

    public GameObject bullet;
    public GameObject muzzleFlash;

    public float shootForce, upwardForce;

    public float timeBetweenShooting, spread, reloadTime, timeBetweenShots, reloadTimeT;
    public int magazineSize, maxAmmo, actualMaxAmmo, bulletPerTap;
    public bool allowButtonHold;
    public int bulletsLeft, bulletsShot, noMoreBullets, difB;

    public bool shooting, readyToShoot, reloading;

    public Camera fpsCam;
    public Transform attackPoint;

    public bool allowInvoke = true;
    public bool noAmmo;
    public bool sound;


    public HUD reloadSlider;

    public GameObject audioClip;
    private void Awake()
    {
        instance = this;
        bulletsLeft = magazineSize;

        maxAmmo = magazineSize * 5;
        actualMaxAmmo = maxAmmo;

        reloadTimeT = reloadTime;

        readyToShoot = true;
    }
    private void Start()
    {
        reloadSlider.SetMaxPistolReloadTime(reloadTime);
    }

    private void Update()
    {
        if (WeaponSwitcher.instance.armaEnMano == 0)
        {
            MyInput();
        }

    }
    private void MyInput()
    {
        if (MenuInGame.instance.pauseActive == false)
        {
            if (allowButtonHold)
            {
                shooting = Input.GetKey(KeyCode.Mouse0);

                if (Input.GetKey(KeyCode.Mouse0))
                {
                    sound = true;
                    EnemyGuardScript.instance.timeToRestartAreaDetection = 5f;
                    EnemySecurityScript.instance.timeToRestartAreaDetection = 5f;
                }
            }
            else
            {
                shooting = Input.GetKeyDown(KeyCode.Mouse0);
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    sound = true;
                    EnemyGuardScript.instance.timeToRestartAreaDetection = 5f;
                    EnemySecurityScript.instance.timeToRestartAreaDetection = 5f;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading)
        {
            Reload();
        }
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0)
        {
            Reload();
        }

        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            bulletsShot = 0;

            Shoot();
        }

        if (actualMaxAmmo <= 0)
        {
            actualMaxAmmo = 0;
            noAmmo = true;
        }
        else
        {
            noAmmo = false;
        }

        if (reloading == true)
        {
            reloadTimeT -= Time.deltaTime;
            reloadSlider.SetReloadTimePistol(reloadTimeT);
        }
    }
    private void Shoot()
    {
        readyToShoot = false;

        Ray ray = fpsCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hit;
        Vector3 targetPoint;
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
        }
        else
        {
            targetPoint = ray.GetPoint(75);
        }

        Vector3 directionWithOutSpread = targetPoint - attackPoint.position;


        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        Vector3 directionWithSpread = directionWithOutSpread + new Vector3(x, y, 0);

        if (WeaponSwitcher.instance.armaEnMano == 0)
        {
            GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
            currentBullet.transform.forward = directionWithSpread.normalized;
            currentBullet.tag = "PistolBullet";
            Instantiate(audioClip);
            Destroy(currentBullet, 3f);

            currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);
            currentBullet.GetComponent<Rigidbody>().AddForce(fpsCam.transform.up * upwardForce, ForceMode.Impulse);
        }

        if(muzzleFlash != null)
        {
            GameObject currentEffect = Instantiate(muzzleFlash, attackPoint.position, muzzleFlash.transform.rotation);
            Destroy(currentEffect, 0.1f);
        }

        bulletsLeft--;
        bulletsShot++;


        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }

        if (bulletsShot < bulletPerTap && bulletsLeft > 0)
        {
            Invoke("Shoot", timeBetweenShots);
        }
    }
    private void ResetShot()
    {
        readyToShoot = true;
        allowInvoke = true;
    }
    private void Reload()
    {
        if (noAmmo == false)
        {
            reloading = true;
            Invoke("ReloadFinished", reloadTime);
        }
        else
        {
            reloading = false;
            CancelInvoke("ReloadFinished");
        }
    }
    private void ReloadFinished()
    {
        if (bulletsLeft <= 0)
        {
            bulletsLeft = magazineSize;
            actualMaxAmmo -= bulletsLeft;
        }
        if (bulletsLeft > 0)
        {
            noMoreBullets = magazineSize - bulletsLeft;

            if (noMoreBullets > actualMaxAmmo)
            {
                difB = actualMaxAmmo;
                bulletsLeft += actualMaxAmmo;
                actualMaxAmmo -= difB;
            }
            else
            {
                bulletsLeft += noMoreBullets;
                actualMaxAmmo -= noMoreBullets;
            }
        }
        reloadTimeT = reloadTime;
        reloading = false;
    }
}
