﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitcher : MonoBehaviour
{
    #region Singleton;

    public static WeaponSwitcher instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    public int armaEnMano = 0;

    void Start()
    {
        armaEnMano = 0;
        armaActual();
    }


    void Update()
    {
        int armaAnterior = armaEnMano;

        if (Input.GetKeyDown(KeyCode.Q) && armaEnMano == 0)
        {
            armaEnMano = 1;
        }
        else if (Input.GetKeyDown(KeyCode.Q) && armaEnMano == 1)
        {
            armaEnMano = 2;
        }
        else if (Input.GetKeyDown(KeyCode.Q) && armaEnMano == 2)
        {
            armaEnMano = 0;
        }

        if (armaAnterior != armaEnMano)
        {
            armaActual();
        }
    }
    void armaActual()
    {
        int i = 0;
        foreach (Transform arma in transform)
        {
            if (i == armaEnMano)
                arma.gameObject.SetActive(true);
            else
                arma.gameObject.SetActive(false);
            i++;
        }
    }
}
