﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGunBullet : MonoBehaviour
{
    #region Singleton

    public static ShotGunBullet instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damageShotGunBullet;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject, 0f);
        }
    }
}
