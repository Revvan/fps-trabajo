﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformDamage : MonoBehaviour
{
    #region Singleton

    public static PlatformDamage instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damage;
    public float timer;
    public bool inPlatform;
    public GameObject text;
    private void Start()
    {
        text.SetActive(false);
    }
    void Update()
    {
        if (inPlatform == true)
        {
            if (FinalBossScript.instance.inPhase == false && FinalBossScript.instance.activePlatf == true || FinalBossScript.instance.inPhaseTwo == false && FinalBossScript.instance.activePlatf == true || FinalBossScript.instance.inPhaseThree == false && FinalBossScript.instance.activePlatf == true)
            {
                timer -= Time.deltaTime; 
                text.SetActive(true);


                if (timer <= 0)
                {
                    PlayerControllerScript.instance.currentHP -= damage;

                    timer = 2f;
                }
            }
        }
        else
        {
            text.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {            
            inPlatform = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inPlatform = false;        
            timer = 2f;
        }
    }
}
