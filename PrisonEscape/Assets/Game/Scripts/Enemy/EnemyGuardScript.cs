﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class EnemyGuardScript : MonoBehaviour
{
    #region Singleton

    public static EnemyGuardScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    public NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    public GameObject muzzleFlash;
    #endregion

    #region TargetReferences
    public Transform player;
    public Transform target;
    #endregion

    #region Gizmos
    public float gizmoRange, dot, fov, dotFov;
    Vector3 v = Vector3.zero;
    float distance = 0.0f;
    public float rangeAttack;
    #endregion

    #region Waypoints
    public Transform[] wayPoints;
    public int wayPointIndex;
    Vector3 wayPointTarget;
    #endregion

    #region Stats
    public int hp, currentHP;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region AreaDetection
    public float timeToRestartAreaDetection = 5f;
    public bool bulletSound = false;

    public bool callAllies = false;
    #endregion

    #region Spawn
    public float timeBetweenSpawns = 5f;
    private Vector3 spawn;
    public Transform SpawnZone;
    public GameObject enemiesToSpawn;
    public int actualAlliesSpawned;
    public int maxAlliesCall = 4;
    #endregion

    #region BoxSpawn
    public Vector3 deathPlace;
    public GameObject box;
    public int cantidad;
    #endregion

    public GameObject audioClip;

    public AudioSource hurtS;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        SpawnZone.name = "SpawnZone";
        UpdateDestination();

        actualBullet = Magazine;
        currentHP = hp;
        healthBar.SetMaxHealth(hp);
    }


    void Update()
    {
        #region Gizmos
        v = player.position - transform.position;
        distance = v.sqrMagnitude;

        v.Normalize();
        dotFov = Mathf.Cos(fov * 0.5f * Mathf.Deg2Rad);
        dot = Vector3.Dot(transform.forward, v);

        if ((distance <= gizmoRange * gizmoRange) && (dot >= dotFov))
        {
            target = player;
            fov = 360f;
            gizmoRange = 15f;
            agent.stoppingDistance = 10f;

            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            agent.SetDestination(target.position);
            AutoLooker();

            if (distance <= rangeAttack * rangeAttack)
            {
                shoot = true;
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
            }

            Debug.DrawLine(transform.position, target.position, Color.red);
        }
        else
        {
            target = null;
            shoot = false;
            fov = 180f;
            gizmoRange = 10f;
            agent.stoppingDistance = 0f;

            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            agent.SetDestination(wayPointTarget);
        }
        #endregion

        if (actualBullet == 0)
        {
            shoot = false;
            reload = true;
        }

        ShootWeapon();
        Reloading();

        if (Vector3.Distance(transform.position, wayPointTarget) < 1)
        {
            IterateWayPointIndex();
            UpdateDestination();
        }

        IncreaseAreaDetection();
        AreaForCallingAllies();
        Spawner();

        if (currentHP < 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            SpawnBox();
            Destroy(this.gameObject);
        }

        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }
    }

    private void UpdateDestination()
    {
        wayPointTarget = wayPoints[wayPointIndex].position;
    }
    private void IterateWayPointIndex()
    {
        wayPointIndex++;
        if(wayPointIndex == wayPoints.Length)
        {
            wayPointIndex = 0;
        }
    }

    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }

    public void AreaForCallingAllies()
    {
        if ((distance <= gizmoRange * gizmoRange) && (dot >= dotFov))
        {
            callAllies = true;
        }
        else
        {
            callAllies = false;
        }
    }
    private void IncreaseAreaDetection()
    {
        if (PistolScript.instance.sound == true)
        {
            gizmoRange = 12f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 10f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                PistolScript.instance.sound = false;
            }
        }
        if (MachineGunScript.instance.sound == true)
        {
            gizmoRange = 15f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 10f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                MachineGunScript.instance.sound = false;
            }
        }
        if (ShotGunScript.instance.sound == true)
        {
            gizmoRange = 20f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 10f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                ShotGunScript.instance.sound = false;
            }
        }
    }
    private void Spawner()
    {
        spawn.x = SpawnZone.position.x;
        spawn.y = SpawnZone.position.y;
        spawn.z = SpawnZone.position.z;

        if (callAllies == true)
        {
            timeBetweenSpawns -= Time.deltaTime;

            if (actualAlliesSpawned < maxAlliesCall && timeBetweenSpawns <= 0)
            {
                Instantiate(enemiesToSpawn, spawn, transform.rotation);
                actualAlliesSpawned++;
                timeBetweenSpawns = 5f;
            }
            if (actualAlliesSpawned >= maxAlliesCall)
            {
                timeBetweenSpawns = 5f;
                callAllies = false;
            }
        }
    }
    private void SpawnBox()
    {
        deathPlace.x = this.transform.position.x;
        deathPlace.y = this.transform.position.y;
        deathPlace.z = this.transform.position.z;

        if (cantidad < 1)
        {
            Instantiate(box, deathPlace, this.transform.rotation);
            cantidad++;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, gizmoRange);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }

        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
