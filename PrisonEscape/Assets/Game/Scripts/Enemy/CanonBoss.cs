﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonBoss : MonoBehaviour
{
    public Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 3);
    }
}
