﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class FinalBossScript : MonoBehaviour
{
    #region Singleton

    public static FinalBossScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion


    #region References
    NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    public GameObject muzzleFlash;
    #endregion

    #region TargetReferences
    public Transform player;
    public Transform target;
    #endregion

    #region Gizmos
    public float gizmoRange, dot, fov, dotFov;
    Vector3 v = Vector3.zero;
    float distance = 0.0f;
    public float rangeAttack;
    #endregion


    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region Stats
    public float currentHP, maxHP;
    #endregion


    #region Phase
    public float firstPhase, secondPhase, thirdPhase;
    public int destroyedEnemies, destroyedEnemies2, destroyedEnemies3;
    public float reduceTakenDamage;
    public bool inPhase, inPhaseTwo, inPhaseThree, activePlatf;
    #endregion

    #region Spawn
    private Vector3 spawn;
    public Transform SpawnZone;
    public Transform SpawnZoneTwo;
    public Transform SpawnZoneThree;
    public Transform SpawnZoneFour;
    public GameObject enemiesToSpawn;
    int actualAlliesSpawned;
    int actualAlliesSpawnedTwo;
    int actualAlliesSpawnedThree;
    int actualAlliesSpawnedFour;

    int actualAlliesSpawned2;
    int actualAlliesSpawnedTwo2;
    int actualAlliesSpawnedThree2;
    int actualAlliesSpawnedFour2;

    int actualAlliesSpawned3;
    int actualAlliesSpawnedTwo3;
    int actualAlliesSpawnedThree3;
    int actualAlliesSpawnedFour3;

    public int maxAlliesCall = 1;
    #endregion

    #region AreaDamage
    public int damageA;
    public float timeToDoDamage, rangeToAreaDamage;
    #endregion

    public GameObject audioClip;
    public AudioSource hurtS;
    public bool activeTP;


    public GameObject text, inPhaseText;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        healthBar.SetMaxHealthBoss(maxHP);

        actualBullet = Magazine;

        firstPhase = maxHP * 0.75f;
        secondPhase = maxHP * 0.5f;
        thirdPhase = maxHP * 0.25f;

        activePlatf = true;


        text.SetActive(false);
        inPhaseText.SetActive(false);
    }
 
    void Update()
    {
        #region Gizmos
        v = player.position - transform.position;
        distance = v.sqrMagnitude;

        v.Normalize();
        dotFov = Mathf.Cos(fov * 0.5f * Mathf.Deg2Rad);
        dot = Vector3.Dot(transform.forward, v);

        if ((distance <= gizmoRange * gizmoRange) && (dot >= dotFov))
        {
            target = player;
            gizmoRange = Mathf.Infinity;
            agent.stoppingDistance = 20f;

            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            agent.SetDestination(target.position);
            AutoLooker();

            if (distance <= rangeAttack * rangeAttack)
            {
                shoot = true;
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
            }
        }
        else
        {
            target = null;
            shoot = false;
            agent.stoppingDistance = 0f;

            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
        }
        #endregion

        if (actualBullet == 0)
        {
            shoot = false;
            reload = true;
        }
        ShootWeapon();
        Reloading();
        AreaDamage();

        #region Phases

        if (currentHP <= firstPhase)
        {
            inPhase = true;
            activePlatf = false;
            inPhaseText.SetActive(true);
            Spawner();
            SpawnerTwo();
            SpawnerThree();
            SpawnerFour();
        }
        if(destroyedEnemies >= 4)
        {
            inPhase = false;
            activePlatf = true;
            inPhaseText.SetActive(false);
        }

        if (currentHP <= secondPhase)
        {
            inPhaseTwo = true;
            activePlatf = false;
            inPhaseText.SetActive(true);
            Spawner();
            SpawnerTwo();
            SpawnerThree();
            SpawnerFour();          
        }
        if (destroyedEnemies2 >= 4)
        {
            inPhaseTwo = false;
            activePlatf = true;
            inPhaseText.SetActive(false);
        }

        if (currentHP <= thirdPhase)
        {
            inPhaseThree = true;
            activePlatf = false;
            inPhaseText.SetActive(true);
            Spawner();
            SpawnerTwo();
            SpawnerThree();
            SpawnerFour();
        }
        if (destroyedEnemies3 >= 4)
        {
            inPhaseThree = false; 
            activePlatf = true;
            inPhaseText.SetActive(false);
        }
        #endregion

        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeFirstAbility = true;
            AbilitiesHUD.instance.image.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            inPhaseText.SetActive(false);
            Destroy(this.gameObject);
        }
        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeFirstAbility = true;
            AbilitiesHUD.instance.image.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            inPhaseText.SetActive(false);
            Destroy(this.gameObject);
        }
    }
    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 5);
    }

    private void AreaDamage()
    {
        if (distance < rangeToAreaDamage * rangeToAreaDamage)
        {
            timeToDoDamage -= Time.deltaTime;
            text.SetActive(true);

            if (timeToDoDamage <= 0)
            {
                PlayerControllerScript.instance.currentHP -= damageA;
                timeToDoDamage = 2f;
            }
        }
        else
        {
            text.SetActive(false);
            timeToDoDamage = 2f;
        }
    }

    #region Spawners
    private void Spawner()
    {
        spawn.x = SpawnZone.position.x;
        spawn.y = SpawnZone.position.y;
        spawn.z = SpawnZone.position.z;

        if (actualAlliesSpawned < maxAlliesCall && inPhase == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawned++;
        }
        if (actualAlliesSpawned2 < maxAlliesCall && inPhaseTwo == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawned2++;
        }
        if (actualAlliesSpawned3 < maxAlliesCall && inPhaseThree == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawned3++;
        }
    }
    private void SpawnerTwo()
    {
        spawn.x = SpawnZoneTwo.position.x;
        spawn.y = SpawnZoneTwo.position.y;
        spawn.z = SpawnZoneTwo.position.z;

        if (actualAlliesSpawnedTwo < maxAlliesCall && inPhase == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedTwo++;
        }
        if (actualAlliesSpawnedTwo2 < maxAlliesCall && inPhaseTwo == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedTwo2++;
        }
        if (actualAlliesSpawnedTwo3 < maxAlliesCall && inPhaseThree == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedTwo3++;
        }
    }
    private void SpawnerThree()
    {
        spawn.x = SpawnZoneThree.position.x;
        spawn.y = SpawnZoneThree.position.y;
        spawn.z = SpawnZoneThree.position.z;

        if (actualAlliesSpawnedThree < maxAlliesCall && inPhase == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedThree++;
        }
        if (actualAlliesSpawnedThree2 < maxAlliesCall && inPhaseTwo == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedThree2++;
        }
        if (actualAlliesSpawnedThree3 < maxAlliesCall && inPhaseThree == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedThree3++;
        }
    }
    private void SpawnerFour()
    {
        spawn.x = SpawnZoneFour.position.x;
        spawn.y = SpawnZoneFour.position.y;
        spawn.z = SpawnZoneFour.position.z;

        if (actualAlliesSpawnedFour < maxAlliesCall && inPhase == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedFour++;
        }
        if (actualAlliesSpawnedFour2 < maxAlliesCall && inPhaseTwo == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedFour2++;
        }
        if (actualAlliesSpawnedFour3 < maxAlliesCall && inPhaseThree == true)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawnedFour3++;
        }
    }
    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
        Gizmos.DrawWireSphere(this.transform.position, gizmoRange);
        Gizmos.DrawWireSphere(this.transform.position, rangeToAreaDamage);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            if (inPhase == true || inPhaseTwo == true || inPhaseThree == true)
            {
                reduceTakenDamage -= PistolMachineGunBullet.instance.damagePistolBullet;
            }
            else
            {
                currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            if (inPhase == true || inPhaseTwo == true || inPhaseThree == true)
            {
                reduceTakenDamage -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            }
            else
            {
                currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            if (inPhase == true || inPhaseTwo == true || inPhaseThree == true)
            {
                reduceTakenDamage -= ShotGunBullet.instance.damageShotGunBullet;
            }
            else
            {
                currentHP -= ShotGunBullet.instance.damageShotGunBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }
            }
        }

        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketBossDamage;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
