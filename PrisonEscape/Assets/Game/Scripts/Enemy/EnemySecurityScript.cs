﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class EnemySecurityScript : MonoBehaviour
{
    #region Singleton

    public static EnemySecurityScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    public NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    public GameObject muzzleFlash;
    #endregion

    #region TargetReferences
    public Transform player;
    public Transform target;
    #endregion

    #region Gizmos
    public float gizmoRange, dot, fov, dotFov;
    Vector3 v = Vector3.zero;
    public float distance = 0.0f;
    public float rangeAttack;
    #endregion

    #region Stats
    public int hp, currentHP;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region AreaDetection
    public float timeToRestartAreaDetection = 5f;
    public bool bulletSound = false;

    public bool callAllies = false;
    #endregion

    #region BoxSpawn
    public Vector3 deathPlace;
    public GameObject box;
    public int cantidad;
    #endregion

    public GameObject audioClip;

    public AudioSource hurtS;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        actualBullet = Magazine;

        currentHP = hp;
        healthBar.SetMaxHealth(hp);
    }

    void Update()
    {
        #region Gizmos
        v = player.position - transform.position;
        distance = v.sqrMagnitude;

        v.Normalize();
        dotFov = Mathf.Cos(fov * 0.5f * Mathf.Deg2Rad);
        dot = Vector3.Dot(transform.forward, v);

        if ((distance <= gizmoRange * gizmoRange) && (dot >= dotFov))
        {
            target = player;
            fov = 360f;
            gizmoRange = 20f;
            agent.stoppingDistance = 15f;

            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            agent.SetDestination(target.position);
            AutoLooker();

            if (distance <= rangeAttack * rangeAttack)
            {
                shoot = true;
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
            }

            Debug.DrawLine(transform.position, target.position, Color.red);
        }
        else
        {
            target = null;
            shoot = false;
            fov = 180f;
            gizmoRange = 15f;
            agent.stoppingDistance = 0f;

            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
        }
        #endregion

        if (actualBullet == 0)
        {
            shoot = false;
            reload = true;
        }

        ShootWeapon();
        Reloading();

        IncreaseAreaDetection();

        if (currentHP <= 0)
        {
            SpawnBox();
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }

        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }
    }
    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    private void IncreaseAreaDetection()
    {
        if (PistolScript.instance.sound == true)
        {
            gizmoRange = 16f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 15f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                PistolScript.instance.sound = false;
            }
        }
        if (MachineGunScript.instance.sound == true)
        {
            gizmoRange = 20f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 15f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                MachineGunScript.instance.sound = false;
            }
        }
        if (ShotGunScript.instance.sound == true)
        {
            gizmoRange = 25f;
            fov = 360f;

            if ((this.distance <= this.gizmoRange * this.gizmoRange) && (this.dot >= this.dotFov) && this.target != null)
                timeToRestartAreaDetection = 5f;
            else
                timeToRestartAreaDetection -= Time.deltaTime;


            if (timeToRestartAreaDetection <= 0)
            {
                gizmoRange = 15f;
                fov = 180f;
                timeToRestartAreaDetection = 5f;
                ShotGunScript.instance.sound = false;
            }
        }
    }

    private void SpawnBox()
    {
        deathPlace.x = this.transform.position.x;
        deathPlace.y = this.transform.position.y;
        deathPlace.z = this.transform.position.z;

        if (cantidad < 1)
        {
            Instantiate(box, deathPlace, this.transform.rotation);
            cantidad++;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, gizmoRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }

            healthBar.SetHealth(currentHP);
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }

            healthBar.SetHealth(currentHP);
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }

            healthBar.SetHealth(currentHP);
        }

        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }

            healthBar.SetHealth(currentHP);
        }
    }
}
