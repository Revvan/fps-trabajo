﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class EnemyToken : MonoBehaviour
{
    #region Singleton

    public static EnemyToken instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    public NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    public GameObject muzzleFlash;
    #endregion

    #region TargetReferences
    public Transform player;

    public float Distance, areaDetection, rangeAttack;
    #endregion

    #region Stats
    public int hp, currentHP;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region BoxSpawn
    public Vector3 deathPlace;
    public GameObject box;
    public int cantidad;
    #endregion

    public GameObject audioClip;

    public AudioSource hurtS;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        actualBullet = Magazine;

        currentHP = hp;
        healthBar.SetMaxHealth(hp);
    }

    void Update()
    {
        Distance = Vector3.Distance(player.position, this.transform.position);
        agent.SetDestination(player.position);
        anim.SetBool("walk", true);
        anim.SetBool("shoot", false);

        if (Distance <= areaDetection)
        {
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);

            if(Distance <= rangeAttack)
            {
                shoot = true;
                agent.stoppingDistance = 15f;
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
            }
        }
        else
        {
            shoot = false;
            agent.stoppingDistance = 0f;
        }

        if (actualBullet <= 0)
        {
            shoot = false;
            reload = true;
        }
        AutoLooker();
        FireGun();
        Reloading();

        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            SpawnBox();
            Destroy(this.gameObject);
        }
        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }
    }

    private void FireGun()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, canon.transform.position, canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 3f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 8f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 2);
    }
    private void SpawnBox()
    {
        deathPlace.x = this.transform.position.x;
        deathPlace.y = this.transform.position.y;
        deathPlace.z = this.transform.position.z;

        if (cantidad < 1)
        {
            Instantiate(box, deathPlace, this.transform.rotation);
            cantidad++;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, areaDetection);
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }

        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
