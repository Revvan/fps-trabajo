﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorDestroyer : MonoBehaviour
{
    public int hp = 10;
    public GameObject Effect;

    private void Update()
    {
        if (hp < 10)
        {
            ThirdBoss.instance.generatorsDestroyed += 1;
            if (Effect != null)
            {
                GameObject currentEffect = Instantiate(Effect, this.transform.position, this.transform.rotation);
                Destroy(currentEffect, 0.2f);
            }
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Rocket")
        {
            hp -= 10;
        }
    }
}
