﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class FourthBoss : MonoBehaviour
{
    #region Singleton

    public static FourthBoss instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public GameObject muzzleFlash;
    public GameObject audioClip;
    public Transform player, target;
    public EnemyHUD healthBar;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    #region Gizmos
    public float gizmoRange, rangeAttack, rangeToAreaDamage, timeToDoDamage, distance;
    public int damageA;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region SpecialAttack
    public float timeToAttack;
    public bool activeDamage;
    #endregion

    #region FirstPhase
    public int umbralHealth, cantidad;
    public bool iFP;
    #endregion
    #region SecondPhase
    public int umbralHealth2, cantidad2;
    public bool iSP;
    #region Spawn
    public float timeBetweenSpawns;
    private Vector3 spawn;
    public Transform SpawnZone;
    public int maxAllies;
    public GameObject enemiesToSpawn;
    #endregion
    #endregion
    #region ThirdPhase
    public int umbralHealth3;
    public GameObject walls;

    #region SpecialAttack
    public GameObject rocket;
    public float timeToSpecial;
    public int damage;
    #endregion
    public bool iTP;
    #endregion

    #region Effects&Sounds
    public GameObject text;
    public GameObject effect;
    public AudioSource hurtS;
    #endregion

    public bool activeTP;
    public GameObject toClose, fPText, sPText, tPText, rText;
    public float timeToNullText, tTNT, tTNT2;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        actualBullet = Magazine;
        healthBar.SetMaxHealthBoss(maxHP);

        umbralHealth = (int)(maxHP * 0.75);
        umbralHealth2 = (int)(maxHP * 0.5);
        umbralHealth3 = (int)(maxHP * 0.25);

        toClose.SetActive(false);
        text.SetActive(false);
        fPText.SetActive(false);
        sPText.SetActive(false);
        tPText.SetActive(false);
        rText.SetActive(false);
    }

    void Update()
    {
        healthBar.SetHealthBoss(currentHP);
        #region SampleDetection
        distance = Vector3.Distance(this.transform.position, player.position);

        if (distance < gizmoRange)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
            target = player;
            agent.stoppingDistance = 25f;
            gizmoRange = 50f;
            agent.SetDestination(target.position);
            LauncheSpecialAttack();

            if (distance <= rangeAttack)
            {
                shoot = true;
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
                anim.SetBool("special", false);
                AutoLooker();
                ShootWeapon();
            }
        }
        else
        {
            target = null;
            agent.stoppingDistance = 0f;
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
        }

        Reloading();
        if (actualBullet <= 0)
        {
            shoot = false;
            reload = true;
        }
        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeFourthAbility = true;
            AbilitiesHUD.instance.image3.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            toClose.SetActive(false);
            text.SetActive(false);
            fPText.SetActive(false);
            sPText.SetActive(false);
            tPText.SetActive(false);
            rText.SetActive(false);
            Destroy(this.gameObject);
        }
        AreaDamage();
        Anims();
        #endregion


        FirstPhase();
        SecondPhase();
        ThirdPhase();

        if (iSP)
        {
            Spawner();
        }
        if (iTP)
        {
            SpecialAttack();
        }

        if (iFP)
        {
            timeToNullText -= Time.deltaTime;
            fPText.SetActive(true);

            if (timeToNullText <= 0)
            {
                fPText.SetActive(false);
            }
        }
        if (iSP)
        {
            tTNT -= Time.deltaTime;
            sPText.SetActive(true);

            if (tTNT <= 0)
            {
                sPText.SetActive(false);
            }
        }

        if (iTP)
        {
            tTNT2 -= Time.deltaTime;
            tPText.SetActive(true);

            if (tTNT2 <= 0)
            {
                tPText.SetActive(false);
            }
        }
    }

    #region Phases
    private void FirstPhase()
    {
        if (currentHP <= umbralHealth)
        {
            iFP = true;
        }
        if (cantidad == 3)
        {
            timeToNullText = 2f;
            fPText.SetActive(false);
            iFP = false;
        }
    }
    private void SecondPhase()
    {
        if (currentHP <= umbralHealth2)
        {
            Spawner();
            iSP = true;
        }
        if (cantidad2 == 3)
        {
            sPText.SetActive(false);
            tTNT = 2f;
            iSP = false;
        }
    }
    private void Spawner()
    {
        spawn.x = SpawnZone.position.x;
        spawn.y = SpawnZone.position.y;
        spawn.z = SpawnZone.position.z;

        timeBetweenSpawns -= Time.deltaTime;

        if (timeBetweenSpawns <= 0 && maxAllies < 5)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            maxAllies += 1;
            timeBetweenSpawns = 7f;
        }
    }
    private void ThirdPhase()
    {
        if (currentHP <= umbralHealth3)
        {
            iTP = true;
            walls.SetActive(true);
        }
    }

    private void SpecialAttack()
    {
        timeToSpecial -= Time.deltaTime;

        if( timeToSpecial < 3f )
        {
            rText.SetActive(true);
        }
        else
        {
            rText.SetActive(false);
        }

        if (timeToSpecial <= 0f)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
            anim.SetBool("special2", true);
            timeToSpecial = 6f;
        }
        else
        {
            anim.SetBool("special2", false);
        }
    }
    public void ShootRocket()
    {
        GameObject currentRocket = Instantiate(rocket, this.canon.transform.position, this.canon.transform.rotation);
        GameObject currentEffect = Instantiate(effect, this.canon.transform.position, this.canon.transform.rotation);
        Destroy(currentEffect, 0.2f);
        Rigidbody rb = currentRocket.GetComponent<Rigidbody>();
        rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
    }
    #endregion

    #region Animations
    private void LauncheSpecialAttack()
    {
        timeToAttack -= Time.deltaTime;

        if(timeToAttack <= 2f)
        {
            text.SetActive(true);
        }

        if (timeToAttack <= 0)
        {
            text.SetActive(false);
            activeDamage = true;
            timeToAttack = 30f;
        }

    }
    private void Anims()
    {
        if(activeDamage == true)
        {
            anim.SetBool("shoot", false);
            anim.SetBool("special", true);

            activeDamage = false;
        }
        else
        {
            anim.SetBool("special", false);
        }
    }
    public void DoDamage()
    {
        if (ManagerAbilities.instance.activeInvulnerability == true)
        {
            PlayerControllerScript.instance.currentHP -= 0;
        }
        else
        {
            PlayerControllerScript.instance.currentHP = 1;
        }
    }
    #endregion

    #region weapon
    private void AreaDamage()
    {
        if (distance < rangeToAreaDamage)
        {
            timeToDoDamage -= Time.deltaTime;
            toClose.SetActive(true);

            if (timeToDoDamage <= 0)
            {
                PlayerControllerScript.instance.currentHP -= damageA;
                timeToDoDamage = 2f;
            }
        }
        else
        {
            toClose.SetActive(false);
            timeToDoDamage = 2f;
        }
    }
    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 7);
    }
    #endregion
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRange);
        Gizmos.DrawWireSphere(this.transform.position, rangeToAreaDamage);
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("Rocket"))
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
