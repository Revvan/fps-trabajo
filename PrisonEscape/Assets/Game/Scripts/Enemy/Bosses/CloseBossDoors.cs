﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseBossDoors : MonoBehaviour
{
    public GameObject Door;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Door.SetActive(true);
        }
    }
}
