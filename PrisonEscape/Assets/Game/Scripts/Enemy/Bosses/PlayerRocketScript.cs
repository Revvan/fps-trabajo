﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerRocketScript : MonoBehaviour
{
    #region Singleton

    public static PlayerRocketScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Rigidbody rb;

    public float explosionRange, distance, timeToExplode;

    public GameObject effect;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        timeToExplode -= Time.deltaTime;
        Explosion();
    }

    private void Explosion()
    {
        if (timeToExplode <= 0)
        {
            if (effect != null)
            {
                GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);

                Destroy(currentEffect, 0.3f);
            }

            Destroy(this.gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, explosionRange);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Untagged")
        {
            if (effect != null)
            {
                GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);
                Destroy(currentEffect, 0.3f);
            }
            Destroy(this.gameObject);
        }
    }
}
