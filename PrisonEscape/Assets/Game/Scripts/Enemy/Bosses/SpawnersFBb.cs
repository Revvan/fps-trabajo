﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnersFBb : MonoBehaviour
{
    #region Singleton

    public static SpawnersFBb instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Vector3 spawnPos;

    public Transform spawn;
    public GameObject enemiesToSpawn;
    public int cant;

    public float timeBetweenSpawns;

    void Update()
    {
        if (FourthBoss.instance.iSP == true)
        {
            Spawner();
        }
    }

    private void Spawner()
    {
        spawnPos.x = spawn.position.x;
        spawnPos.y = spawn.position.y;
        spawnPos.z = spawn.position.z;

        timeBetweenSpawns -= Time.deltaTime;

        if (timeBetweenSpawns <= 0 && cant == 0)
        {
            Instantiate(enemiesToSpawn, spawnPos, transform.rotation);
            cant += 1;
            timeBetweenSpawns = 3f;
        }
    }
}
