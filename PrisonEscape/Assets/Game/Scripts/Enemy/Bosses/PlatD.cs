﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatD : MonoBehaviour
{
    #region Singleton

    public static PlatD instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damage;
    public float timer;
    public bool inPlatform;
    public GameObject text;

    private void Start()
    {
        text.SetActive(false);
    }
    void Update()
    {
        if (inPlatform)
        {
            if (ThirdBoss.instance.inPhase == false)
            {
                timer -= Time.deltaTime;
                text.SetActive(true);

                if (timer <= 0)
                {
                    PlayerControllerScript.instance.currentHP -= damage;
                    timer = 2f;
                }
            }
        }
        else
        {
            text.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inPlatform = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inPlatform = false;
            timer = 2f;
        }
    }
}
