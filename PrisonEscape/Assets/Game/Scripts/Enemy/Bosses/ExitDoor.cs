﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitDoor : MonoBehaviour
{
    #region Singleton

    public static ExitDoor instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public GameObject bossesdefeated;
    public int count;
    public float timer;

    private void Start()
    {
        bossesdefeated.SetActive(false);
    }
    private void Update()
    {
        if (count == 4)
        {
            bossesdefeated.SetActive(true);

            timer -= Time.deltaTime;
        }
        if (timer <= 0)
        {
            bossesdefeated.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (count == 4)
            {
                MenuInGame.instance.win = true;
            }
        }

    }
}
