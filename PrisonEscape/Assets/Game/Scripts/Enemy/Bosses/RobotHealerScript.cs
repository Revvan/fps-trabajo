﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class RobotHealerScript : MonoBehaviour
{
    #region Singleton

    public static RobotHealerScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public Transform target;
    public EnemyHUD healthBar;
    #endregion

    #region Stats
    public int currentHP, maxHP, regenerateHP;
    #endregion

    #region Gizmos
    public float gizmoRange, healRange, distance;
    #endregion

    #region BoxSpawn
    public Vector3 deathPlace;
    public GameObject box;
    public int cantidad;
    #endregion

    public AudioSource hurtS;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Boss").transform;

        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
    }

    void Update()
    {
        distance = Vector3.Distance(this.transform.position, target.position);

        if (distance < gizmoRange)
        {
            agent.SetDestination(target.position);
            agent.stoppingDistance = 5f;
            AutoLooker();

            anim.SetBool("walk", true);
            anim.SetBool("special", false);

            if (distance <= healRange)
            {
                anim.SetBool("walk", false);
                anim.SetBool("special", true);
            }
        }

        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            SpawnBox();
            Destroy(this.gameObject);
        }
        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }

        if (ThirdBoss.instance.inPhase)
        {
            agent.speed = 6f;
        }
        else
        {
            agent.speed = 3f;
        }

        if(ThirdBoss.instance.currentHP <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    private void AutoLooker()
    {
        Vector3 direccion = (target.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 5);
    }
    public void HealBoss()
    {
        if(ThirdBoss.instance.inPhase == true)
        {
            ThirdBoss.instance.absorbHeal += regenerateHP;
        }
        else
        {
            ThirdBoss.instance.currentHP += regenerateHP;
        }

        Destroy(this.gameObject);
    }
    private void SpawnBox()
    {
        deathPlace.x = this.transform.position.x;
        deathPlace.y = this.transform.position.y;
        deathPlace.z = this.transform.position.z;

        if (cantidad < 1)
        {
            Instantiate(box, deathPlace, this.transform.rotation);
            cantidad++;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealth(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealth(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealth(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
