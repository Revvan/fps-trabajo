﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Animations;

[RequireComponent(typeof(NavMeshAgent))]
public class SecondBoss : MonoBehaviour
{
    #region Singleton

    public static SecondBoss instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public GameObject muzzleFlash;
    public GameObject audioClip;
    public Transform player, target;
    public EnemyHUD healthBar;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    #region Gizmos
    public float gizmoRange, rangeAttack, distance;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region SpecialAttack
    public GameObject rocket;
    public float timeToSpecial;
    public int damage;
    #endregion

    #region AreaDamage
    public int damageA;
    public float timeToDoDamage, rangeToAreaDamage;
    #endregion

    #region Spawn
    public float timeBetweenSpawns;
    private Vector3 spawn;
    public Transform SpawnZone;
    public GameObject enemiesToSpawn;
    public int actualAlliesSpawned;
    #endregion

    public AudioSource hurtS;
    public GameObject effect;

    public bool activeTP;
    public GameObject text, rocketW;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        actualBullet = Magazine;
        healthBar.SetMaxHealthBoss(maxHP);

        text.SetActive(false);
    }
    void Update()
    {
        #region SampleDetection
        distance = Vector3.Distance(this.transform.position, player.position);

        if(distance < gizmoRange)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
            target = player;
            agent.stoppingDistance = 20f;
            gizmoRange = 50f;
            agent.SetDestination(target.position);
            Spawner();

            if (distance <= rangeAttack)
            {
                shoot = true;
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", true);
                anim.SetBool("special", false);
                AutoLooker();
                ShootWeapon();
            }
            else
            {
                shoot = false;
                anim.SetBool("idle", false);
                anim.SetBool("walk", true);
                anim.SetBool("shoot", false);
                anim.SetBool("special", false);
            }
        }
        else
        {
            target = null;
            agent.stoppingDistance = 0f;
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
        }

        Reloading();
        SpecialAttack();
        #endregion
        if(actualBullet <= 0)
        {
            shoot = false;
            reload = true;     
        }
        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeSecondAbility = true;
            AbilitiesHUD.instance.image1.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            rocketW.SetActive(false);
            Destroy(this.gameObject);
        }
        AreaDamage();

        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeSecondAbility = true;
            AbilitiesHUD.instance.image1.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            rocketW.SetActive(false);
            Destroy(this.gameObject);
        }

        if (timeToSpecial <= 3)
        {
            rocketW.SetActive(true);
        }
        else
        {
            rocketW.SetActive(false);
        }
    }
    private void Spawner()
    {
        spawn.x = Random.Range(-128f, -159f);
        spawn.y = 7f;
        spawn.z = Random.Range(246f, 282f);

        timeBetweenSpawns -= Time.deltaTime;

        if (timeBetweenSpawns <= 0)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);
            actualAlliesSpawned++;
            timeBetweenSpawns = 15f;
        }
    }
    private void AreaDamage()
    {
        if (distance < rangeToAreaDamage)
        {
            timeToDoDamage -= Time.deltaTime;
            text.SetActive(true);

            if (timeToDoDamage <= 0)
            {
                PlayerControllerScript.instance.currentHP -= damageA;
                timeToDoDamage = 2f;
            }
        }
        else
        {
            text.SetActive(false);
            timeToDoDamage = 2f;
        }
    }
    private void SpecialAttack()
    {
        if (distance <= gizmoRange)
        {
            timeToSpecial -= Time.deltaTime;

            if (timeToSpecial <= 0f)
            {
                anim.SetBool("idle", false);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", false);
                anim.SetBool("special", true);
                timeToSpecial = 10f;
            }
        }
    }
    public void ShootRocket()
    {
        GameObject currentRocket = Instantiate(rocket, this.canon.transform.position, this.canon.transform.rotation);
        Rigidbody rb = currentRocket.GetComponent<Rigidbody>();
        GameObject currentEffect = Instantiate(effect, this.canon.transform.position, this.canon.transform.rotation);
        Destroy(currentEffect, 0.2f);
        rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
    }
    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 5);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRange);
        Gizmos.DrawWireSphere(this.transform.position, rangeToAreaDamage);
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
