﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportConsoleScript : MonoBehaviour
{
    #region Singleton

    public static TeleportConsoleScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public Transform tpPos;
    public Transform playerPos;  

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            this.playerPos.position = this.tpPos.position;
        }
    }
}
