﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket1 : MonoBehaviour
{
    Rigidbody rb;

    public Transform player;

    public float explosionRange, distance, timeToExplode;

    public GameObject effect;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Update()
    {
        timeToExplode -= Time.deltaTime;
        Explosion();
    }

    private void Explosion()
    {
        distance = Vector3.Distance(this.transform.position, player.position);

        if (timeToExplode <= 0)
        {
            if (effect != null)
            {
                GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);
                Destroy(currentEffect, 0.3f);
            }
            Destroy(this.gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, explosionRange);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerControllerScript.instance.currentHP -= FourthBoss.instance.damage;
            if (effect != null)
            {
                GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);
                Destroy(currentEffect, 0.3f);
            }
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Untagged")
        {
            if (distance < explosionRange)
            {
                PlayerControllerScript.instance.currentHP -= FourthBoss.instance.damage;
                if (effect != null)
                {
                    GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);
                    Destroy(currentEffect, 0.3f);
                }
                Destroy(this.gameObject);
            }
            else
            {
                if (effect != null)
                {
                    GameObject currentEffect = Instantiate(effect, this.transform.position, this.transform.rotation);
                    Destroy(currentEffect, 0.3f);
                }
                Destroy(this.gameObject);
            }
        }
    }
}
