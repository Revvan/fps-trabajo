﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourthBossBullet : MonoBehaviour
{
    #region Singleton

    public static FourthBossBullet instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damageBossBullet;

    private void Update()
    {
        if (FourthBoss.instance.iFP == false) 
        {
            damageBossBullet = 10;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject, 0.5f);
        }
    }
}
