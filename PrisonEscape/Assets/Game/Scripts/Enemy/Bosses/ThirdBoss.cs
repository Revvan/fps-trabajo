﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class ThirdBoss : MonoBehaviour
{
    #region Singleton

    public static ThirdBoss instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public GameObject muzzleFlash;
    public GameObject audioClip;
    public Transform player, target;
    public EnemyHUD healthBar;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    #region Gizmos
    public float gizmoRange, rangeAttack, rangeToAreaDamage, timeToDoDamage, distance;
    public int damageA;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region Spawn
    public float timeBetweenSpawns, timeBetweenSpawns1;
    private Vector3 spawn, spawn1;
    public Transform SpawnZone, SpawnZone1;
    public GameObject enemiesToSpawn;
    #endregion


    #region SpecialAttack
    public int enterInPhase, canActivePhase, newHP, generatorsDestroyed, absorbHeal;
    public float durationPhase, stunDuration;
    public bool inPhase, stunned;
    #endregion

    public AudioSource hurtS;
    public bool activeTP;
    public GameObject text, inPhaseText, stunT, endPhaseT;
    public float timeToText;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        actualBullet = Magazine;
        healthBar.SetMaxHealthBoss(maxHP);

        enterInPhase = (int)(maxHP * 0.5);

        text.SetActive(false);
        stunT.SetActive(false);
        inPhaseText.SetActive(false);
        endPhaseT.SetActive(false);
    }


    void Update()
    {
        #region SampleDetection
        distance = Vector3.Distance(this.transform.position, player.position);

        if (distance < gizmoRange)
        {
            anim.SetBool("idle", false);
            anim.SetBool("walk", true);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
            target = player;
            agent.stoppingDistance = 25f;
            gizmoRange = 50f;
            agent.SetDestination(target.position);
            Spawner();
            Spawner1();

            if (!stunned)
            {
                if (distance <= rangeAttack)
                {
                    if (inPhase == false)
                    {
                        shoot = true;
                        anim.SetBool("idle", false);
                        anim.SetBool("walk", false);
                        anim.SetBool("shoot", true);
                        anim.SetBool("special", false);
                        AutoLooker();
                        ShootWeapon();
                    }
                    else
                    {
                        shoot = false;
                    }
                }
                else
                {
                    shoot = false;
                    anim.SetBool("idle", false);
                    anim.SetBool("walk", true);
                    anim.SetBool("shoot", false);
                    anim.SetBool("special", false);
                }
            }
            else
            {
                anim.SetBool("idle", true);
                anim.SetBool("walk", false);
                anim.SetBool("shoot", false);
                anim.SetBool("special", false);
            }
        }
        else
        {
            target = null;
            agent.stoppingDistance = 0f;
            anim.SetBool("idle", true);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
            anim.SetBool("special", false);
        }

        Reloading();
        #endregion

        if (actualBullet <= 0)
        {
            shoot = false;
            reload = true;
        }
        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //ManagerAbilities.instance.activeThirdAbility = true;
            Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            AbilitiesHUD.instance.image2.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            stunT.SetActive(false);
            inPhaseText.SetActive(false);
            Destroy(this.gameObject);
        }
        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValueForKillingBoss;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            ManagerAbilities.instance.activeSecondAbility = true;
            AbilitiesHUD.instance.image2.SetActive(true);
            ExitDoor.instance.count += 1;
            activeTP = true;
            text.SetActive(false);
            stunT.SetActive(false);
            inPhaseText.SetActive(false);
            Destroy(this.gameObject);
        }

        if (canActivePhase == 1)
        {
            timeToText -= Time.deltaTime;

            if (timeToText <= 0)
            {
                endPhaseT.SetActive(false);
            }
        }

        AreaDamage();
        ActiveAbility();
        Stun();
    }

    private void ActiveAbility()
    {
        if (currentHP <= enterInPhase && canActivePhase == 0)
        {
            inPhase = true;
            durationPhase -= Time.deltaTime;
            inPhaseText.SetActive(true);

            anim.SetBool("idle", false);
            anim.SetBool("walk", false);
            anim.SetBool("shoot", false);
            anim.SetBool("special", true);

            if (durationPhase < 25f)
            {
                inPhaseText.SetActive(false);
            }

            if (durationPhase <= 0)
            {
                currentHP += absorbHeal;
                canActivePhase += 1;
                inPhase = false;
                endPhaseT.SetActive(true);
                anim.SetBool("special", false);
            }
        }
    }
    private void Stun()
    {
        if (inPhase)
        {
            if (generatorsDestroyed == 4)
            {
                inPhase = false;
                stunned = true;
                canActivePhase += 1;
            }
        }
        if (stunned)
        {
            stunDuration -= Time.deltaTime;
            stunT.SetActive(true);

            if (stunDuration <= 0)
            {
                stunned = false;
                stunT.SetActive(false);
                stunDuration = 5f;
            }
        }
    }
    private void Spawner()
    {
        spawn.x = Random.Range(-293f, -299f);
        spawn.y = -30f;
        spawn.z = Random.Range(109f, 58f);

        timeBetweenSpawns -= Time.deltaTime;

        if (timeBetweenSpawns <= 0)
        {
            Instantiate(enemiesToSpawn, spawn, transform.rotation);

            if (inPhase)
            {
                timeBetweenSpawns = 5f;
            }
            else
            {
                timeBetweenSpawns = 15f;
            }
        }
    }
    private void Spawner1()
    {
        spawn1.x = Random.Range(-346f, -340f);
        spawn1.y = -30f;
        spawn1.z = Random.Range(58f, 111f);

        timeBetweenSpawns1 -= Time.deltaTime;

        if (timeBetweenSpawns1 <= 0)
        {
            Instantiate(enemiesToSpawn, spawn1, transform.rotation);
            
            if (inPhase)
            {
                timeBetweenSpawns1 = 5f;
            }
            else
            {
                timeBetweenSpawns1 = 15f;
            }
        }


    }
    private void AreaDamage()
    {
        if (distance < rangeToAreaDamage)
        {
            timeToDoDamage -= Time.deltaTime;
            text.SetActive(true);

            if (timeToDoDamage <= 0)
            {
                PlayerControllerScript.instance.currentHP -= damageA;
                timeToDoDamage = 2f;
            }
        }
        else
        {
            text.SetActive(false);
            timeToDoDamage = 2f;
        }
    }
    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 5);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, gizmoRange);
        Gizmos.DrawWireSphere(this.transform.position, rangeToAreaDamage);
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            if (inPhase == true)
            {
                newHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            }
            else
            {
                currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }

                if (stunned)
                {
                    currentHP -= PistolMachineGunBullet.instance.damagePistolBullet * 2;
                    healthBar.SetHealthBoss(currentHP);
                    if (MenuInGame.instance.pauseActive == false)
                    {
                        hurtS.Play();
                    }
                    else
                    {
                        hurtS.Stop();
                    }
                }
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            if (inPhase)
            {
                newHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            }
            else
            {
                currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }

                if (stunned)
                {
                    currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet * 2;
                    healthBar.SetHealthBoss(currentHP);
                    if (MenuInGame.instance.pauseActive == false)
                    {
                        hurtS.Play();
                    }
                    else
                    {
                        hurtS.Stop();
                    }
                }
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            if (inPhase)
            {
                newHP -= ShotGunBullet.instance.damageShotGunBullet;
            }
            else
            {
                currentHP -= ShotGunBullet.instance.damageShotGunBullet;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }

                if (stunned)
                {
                    currentHP -= ShotGunBullet.instance.damageShotGunBullet * 2;
                    healthBar.SetHealthBoss(currentHP);
                    if (MenuInGame.instance.pauseActive == false)
                    {
                        hurtS.Play();
                    }
                    else
                    {
                        hurtS.Stop();
                    }
                }
            }
        }
        if (collision.gameObject.CompareTag("Rocket"))
        {
            if (inPhase)
            {
                newHP -= ManagerAbilities.instance.rocketDamage;
            }
            else
            {
                currentHP -= ManagerAbilities.instance.rocketDamage;
                healthBar.SetHealthBoss(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurtS.Play();
                }
                else
                {
                    hurtS.Stop();
                }

                if (stunned)
                {
                    currentHP -= ManagerAbilities.instance.rocketDamage * 2;
                    healthBar.SetHealthBoss(currentHP);
                    if (MenuInGame.instance.pauseActive == false)
                    {
                        hurtS.Play();
                    }
                    else
                    {
                        hurtS.Stop();
                    }
                }
            }
        }
    }
}
