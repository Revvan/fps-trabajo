﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class RobotShieldHealer : MonoBehaviour
{
    #region Singleton

    public static RobotShieldHealer instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    public int heal;

    public AudioSource hurtS;
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();

        currentHP = maxHP;
    }
    private void Update()
    {
        if (currentHP <= 0)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);

            if (FourthBoss.instance.iFP == true)
            {
                FourthBoss.instance.cantidad += 1;
            }
            if(FourthBoss.instance.iSP == true)
            {
                FourthBoss.instance.cantidad2 += 1;
            }

            Destroy(this.gameObject);
        }
        if (ManagerAbilities.instance.activeLastAbility == true)
        {
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            Destroy(this.gameObject);
        }
    }
    public void HealBoss()
    {
        FourthBoss.instance.currentHP += heal;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("Rocket"))
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealthBoss(currentHP);
            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
