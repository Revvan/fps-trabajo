﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldGeneratorScript : MonoBehaviour
{
    public GameObject generator;
    public Transform myPos;

    public int cant;

    void Update()
    {
        if (ThirdBoss.instance.inPhase == true)
        {
            if (cant == 0)
            {
                Instantiate(generator, myPos.position, myPos.rotation);
                cant += 1;
            }
        }
    }
}
