﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;


[RequireComponent(typeof(NavMeshAgent))]
public class RobotShieldScript : MonoBehaviour
{
    #region Singleton

    public static RobotShieldScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    #region References
    NavMeshAgent agent;
    Animator anim;
    public EnemyHUD healthBar;
    public GameObject muzzleFlash;
    #endregion

    #region TargetReferences
    public Transform player;
    public Transform target;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    #region WeaponVariables
    public GameObject canon;
    public GameObject bullet;
    public int actualBullet;
    public int Magazine = 15;
    public float timeBetweenShooting;
    public float recharge;
    public float shootForce;
    public float rangeAttack;

    public bool shoot = false;
    public bool reload = false;
    #endregion

    #region BoxSpawn
    public Vector3 deathPlace;
    public GameObject box;
    public int cantidad;
    #endregion

    public GameObject audioClip;

    public AudioSource hurtS;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        currentHP = maxHP;
        actualBullet = Magazine;
        healthBar.SetMaxHealth(maxHP);
    }

    void Update()
    {
        float distance = Vector3.Distance(this.transform.position, player.position);

        target = player;
        agent.SetDestination(target.position);

        if (distance <= rangeAttack)
        {
            shoot = true;
            agent.stoppingDistance = 20f;
        }
        else
        {
            shoot = false;
        }

        ShootWeapon();
        anim.SetBool("shoot", true);
        Reloading();
        AutoLooker();

        if (actualBullet == 0)
        {
            shoot = false;
            reload = true;
        }

        if (currentHP <= 0)
        {
            if (FinalBossScript.instance.inPhase == true)
            {
                FinalBossScript.instance.destroyedEnemies++;
            }

            if (FinalBossScript.instance.inPhaseTwo == true)
            {
                FinalBossScript.instance.destroyedEnemies2++;
            }

            if (FinalBossScript.instance.inPhaseThree == true)
            {
                FinalBossScript.instance.destroyedEnemies3++;
            }
            //ScoreScript.instance.actualScore += ScoreScript.instance.scoreValue;
            //Interlude.InterludeManager.UpdateScore(ScoreScript.instance.actualScore);
            SpawnBox();
            Destroy(this.gameObject);
        }
    }

    private void ShootWeapon()
    {
        if (shoot == true)
        {
            timeBetweenShooting -= Time.deltaTime;

            if (timeBetweenShooting <= 0)
            {
                GameObject currentBullet = Instantiate(bullet, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentBullet.GetComponent<Rigidbody>();
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                Instantiate(audioClip);
                Destroy(currentBullet, 1.5f);

                if (muzzleFlash != null)
                {
                    GameObject currentEffect = Instantiate(muzzleFlash, canon.transform.position, muzzleFlash.transform.rotation);
                    Destroy(currentEffect, 0.1f);
                }

                actualBullet--;
                timeBetweenShooting = 1f;
            }
        }
    }
    private void Reloading()
    {
        if (reload == true)
        {
            recharge -= Time.deltaTime;

            if (recharge <= 0)
            {
                actualBullet = Magazine;
                recharge = 5f;
                reload = false;
            }
        }
    }
    public void AutoLooker()
    {
        Vector3 direccion = (player.position - transform.position).normalized;
        Quaternion mirar = Quaternion.LookRotation(new Vector3(direccion.x, direccion.y, direccion.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, mirar, Time.deltaTime * 3);
    }
    private void SpawnBox()
    {
        deathPlace.x = this.transform.position.x;
        deathPlace.y = this.transform.position.y;
        deathPlace.z = this.transform.position.z;

        if (cantidad < 1)
        {
            Instantiate(box, deathPlace, this.transform.rotation);
            cantidad++;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(canon.transform.position, canon.transform.forward * rangeAttack);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("PistolBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damagePistolBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("MachineGunBullet"))
        {
            currentHP -= PistolMachineGunBullet.instance.damageMachineGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.CompareTag("ShotGunBullet"))
        {
            currentHP -= ShotGunBullet.instance.damageShotGunBullet;
            healthBar.SetHealth(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
        if (collision.gameObject.tag == "Rocket")
        {
            currentHP -= ManagerAbilities.instance.rocketDamage;
            healthBar.SetHealthBoss(currentHP);

            if (MenuInGame.instance.pauseActive == false)
            {
                hurtS.Play();
            }
            else
            {
                hurtS.Stop();
            }
        }
    }
}
