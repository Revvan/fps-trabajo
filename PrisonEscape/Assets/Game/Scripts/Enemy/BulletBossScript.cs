﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBossScript : MonoBehaviour
{
    #region Singleton

    public static BulletBossScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damageBossBullet;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject, 0.5f);
        }
    }
}
