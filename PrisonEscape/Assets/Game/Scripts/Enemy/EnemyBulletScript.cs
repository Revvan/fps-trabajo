﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    #region Singleton

    public static EnemyBulletScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public int damageEnemyBullet;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject, 0.5f);
        }
    }
}
