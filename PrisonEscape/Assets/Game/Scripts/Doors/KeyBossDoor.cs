﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBossDoor : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //PlayerControllerScript.instance.myBossKey++;
            Destroy(this.gameObject);
        }
    }
}
