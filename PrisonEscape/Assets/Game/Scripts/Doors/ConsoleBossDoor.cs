﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleBossDoor : MonoBehaviour
{
    public GameObject BossDoor;
    public bool canOpenBossDoor;

    public GameObject text;
    public Text textT;

    private void Start()
    {
        text.SetActive(false);
        textT.gameObject.SetActive(false);
    }
    private void Update()
    {
        OpenDoor();
    }
    void OpenDoor()
    {
        if (canOpenBossDoor == true)
        {
            text.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F))
            {
                BossDoor.SetActive(false);
                text.SetActive(false);
                textT.gameObject.SetActive(false);
                Destroy(this.gameObject, 1f);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                textT.text = "You need the two Red Keys to open the door";
                textT.gameObject.SetActive(true);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canOpenBossDoor = true;
             //&& PlayerControllerScript.instance.haveBossKey == true
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            text.SetActive(false);
            textT.gameObject.SetActive(false);
            canOpenBossDoor = false;
        }
    }
}
