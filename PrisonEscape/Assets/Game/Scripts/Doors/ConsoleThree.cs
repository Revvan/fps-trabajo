﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleThree : MonoBehaviour
{
    #region Singleton

    public static ConsoleThree instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public Transform tpPos;
    public Transform playerPos;

    private void Update()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            this.playerPos.position = this.tpPos.position;
        }
    }
}
