﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleDoorScript : MonoBehaviour
{
    public GameObject Door;
    public bool canOpenDoor;

    public GameObject text;
    public Text textT;

    private void Start()
    {
        text.SetActive(false);
        textT.gameObject.SetActive(false);
    }
    private void Update()
    {
        OpenDoor();
    }
    void OpenDoor()
    {
        if(canOpenDoor == true)
        {
            text.SetActive(true);
            if (Input.GetKeyDown(KeyCode.F))
            {
                Door.SetActive(false);
                text.SetActive(false);
                textT.gameObject.SetActive(false);
                Destroy(this.gameObject, 1f);
            }
        }
        else if(canOpenDoor == false)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                textT.text = "You need the Black Key to open the door";
                textT.gameObject.SetActive(true);
            }  
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            canOpenDoor = true;
             //&& PlayerControllerScript.instance.haveKey == true
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            text.SetActive(false);
            textT.gameObject.SetActive(false);
            canOpenDoor = false;
        }
    }
}
