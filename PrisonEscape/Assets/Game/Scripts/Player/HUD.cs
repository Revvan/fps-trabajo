﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public Slider slider;
    public Slider pistolReload;
    public GameObject containerPistolReload;
    public Slider shotgunReload;
    public GameObject containerShotGunReload;
    public Slider rifleReload;
    public GameObject containerRifleReload;

    //public Text redKeyT;
    //public Text blackKeyT;
    public Text ammoT;
    public Text weaponT;
    public Text reloadT;
    public Text hp;


    private void Start()
    {
        pistolReload.gameObject.SetActive(false);
        shotgunReload.gameObject.SetActive(false);
        rifleReload.gameObject.SetActive(false);
        containerPistolReload.SetActive(false);
        containerRifleReload.SetActive(false);
        containerShotGunReload.SetActive(false);




        //redKeyT.gameObject.SetActive(true);
        //blackKeyT.gameObject.SetActive(true);
        ammoT.gameObject.SetActive(true);
        weaponT.gameObject.SetActive(true);
    }
    private void Update()
    {
        hp.text = PlayerControllerScript.instance.currentHP + " / " + PlayerControllerScript.instance.maxHP;
        //redKeyT.text = "Red Key: " + PlayerControllerScript.instance.myBossKey.ToString();
        //blackKeyT.text = "Black Key: " + PlayerControllerScript.instance.myKey.ToString();

        if (WeaponSwitcher.instance.armaEnMano == 0)
        {
            weaponT.text = "Pistol";
            ammoT.text = PistolScript.instance.bulletsLeft + " / " + PistolScript.instance.actualMaxAmmo;
            reloadT.text = PistolScript.instance.reloadTimeT.ToString("0");
            if (PistolScript.instance.reloading == true)
            {
                containerPistolReload.SetActive(true);
                reloadT.gameObject.SetActive(true);
                ammoT.gameObject.SetActive(false);
            }
            else
            {
                containerPistolReload.SetActive(false);
                reloadT.gameObject.SetActive(false);
                ammoT.gameObject.SetActive(true);
            }
        }
        else
        {
            containerPistolReload.SetActive(false);
        }

        if (WeaponSwitcher.instance.armaEnMano == 1)
        {
            weaponT.text = "MachineGun";
            ammoT.text = MachineGunScript.instance.bulletsLeft + " / " + MachineGunScript.instance.actualMaxAmmo;
            reloadT.text = MachineGunScript.instance.reloadTimeT.ToString("0");
            if (MachineGunScript.instance.reloading == true)
            {
                containerRifleReload.SetActive(true);
                reloadT.gameObject.SetActive(true);
                ammoT.gameObject.SetActive(false);
            }
            else
            {
                containerRifleReload.SetActive(false);
                reloadT.gameObject.SetActive(false);
                ammoT.gameObject.SetActive(true);
            }
        }
        else
        {
            containerRifleReload.SetActive(false);
        }

        if (WeaponSwitcher.instance.armaEnMano == 2)
        {
            weaponT.text = "ShotGun";
            ammoT.text = ShotGunScript.instance.bulletsLeft + " / " + ShotGunScript.instance.actualMaxAmmo;
            reloadT.text = ShotGunScript.instance.reloadTimeT.ToString("0");
            if (ShotGunScript.instance.reloading == true)
            {
                containerShotGunReload.SetActive(true);
                reloadT.gameObject.SetActive(true);
                ammoT.gameObject.SetActive(false);
            }
            else
            {
                containerShotGunReload.SetActive(false);
                reloadT.gameObject.SetActive(false);
                ammoT.gameObject.SetActive(true);
            }
        }
        else
        {
            containerShotGunReload.SetActive(false);
        }
    }

    public void SetMaxShotgunReloadTime(float time)
    {
        shotgunReload.maxValue = time;
        shotgunReload.value = time;
    }
    public void SetReloadTimeShotgun(float time)
    {
        shotgunReload.value = time;
    }


    public void SetMaxRifleReloadTime(float time)
    {
        rifleReload.maxValue = time;
        rifleReload.value = time;
    }
    public void SetReloadTimeRifle(float time)
    {
        rifleReload.value = time;
    }


    public void SetMaxPistolReloadTime(float time)
    {
        pistolReload.maxValue = time;
        pistolReload.value = time;
    }
    public void SetReloadTimePistol(float time)
    {
        pistolReload.value = time;
    }


    public void SetHealth(int health)
    {
        slider.value = health;
    }
    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        slider.value = health;
    }
}
