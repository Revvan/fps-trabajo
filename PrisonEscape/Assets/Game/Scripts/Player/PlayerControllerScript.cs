﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


[RequireComponent(typeof(CharacterController))]
public class PlayerControllerScript : MonoBehaviour
{
    #region Singleton;

    public static PlayerControllerScript instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion;

    #region References
    public CharacterController controller;
    #endregion

    #region Variables
    public float speed;
    public float runSpeed;
    public float gravity = -19.62f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    #endregion

    #region Stats
    public int currentHP, maxHP;
    #endregion

    //#region Key&Door
    //public int myKey;
    //public bool haveKey;
    //public int myBossKey;
    //public bool haveBossKey;
    //#endregion

    public HUD healthBar;

    
    public float wait;
    public bool isMoving, isRun;

    public AudioSource sounds, hurt;
    public AudioSource audioSource;
    public AudioClip ammoFill;

    public Transform pos;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        controller.detectCollisions = true;

        currentHP = maxHP;
        healthBar.SetMaxHealth(maxHP);
    }


    void Update()
    {
        #region MovementSector
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        


        if (MenuInGame.instance.pauseActive == false)
        {
            if (Input.GetKey(KeyCode.W) && isGrounded || Input.GetKey(KeyCode.A) && isGrounded || Input.GetKey(KeyCode.D) && isGrounded || Input.GetKey(KeyCode.S) && isGrounded || Input.GetKey(KeyCode.UpArrow) && isGrounded || Input.GetKey(KeyCode.LeftArrow) && isGrounded || Input.GetKey(KeyCode.RightArrow) && isGrounded || Input.GetKey(KeyCode.DownArrow) && isGrounded)
            {
                isMoving = true;

                if (isMoving)
                {
                    wait -= Time.deltaTime;

                    if (wait <= 0)
                    {
                        sounds.Play();
                        wait = 0.5f;
                    }
                }

            }
            else
            {
                sounds.Stop();
                isMoving = false;
                wait = 0.5f;
            }
        }



        Vector3 move = transform.right * x + transform.forward * z;

        if (Input.GetKey(KeyCode.LeftShift) && isGrounded)
        {
            controller.Move(move * runSpeed * Time.deltaTime);
        }
        else
        {
            controller.Move(move * speed * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
        #endregion

        //#region CheckKeys
        //if (myKey >= 1)
        //{
        //    haveKey = true;
        //}
        //if(myBossKey >= 2)
        //{
        //    haveBossKey = true;
        //}
        //#endregion

        healthBar.SetHealth(currentHP);
        if (currentHP <= 0)
        {
            MenuInGame.instance.lost = true;
            Destroy(this.gameObject);
        }

        if(FinalBossScript.instance.activeTP == true || SecondBoss.instance.activeTP == true || ThirdBoss.instance.activeTP == true || FourthBoss.instance.activeTP == true)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                this.transform.position = pos.position;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            if (ManagerAbilities.instance.activeInvulnerability == true)
            {
                ManagerAbilities.instance.absordDamage -= EnemyBulletScript.instance.damageEnemyBullet;
            }
            else
            {
                currentHP -= EnemyBulletScript.instance.damageEnemyBullet;
                healthBar.SetHealth(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurt.Play();
                }
                else
                {
                    hurt.Stop();
                }
            }
        }
        if (collision.gameObject.CompareTag("EnemyBossBullet"))
        {
            if (ManagerAbilities.instance.activeInvulnerability == true)
            {
                ManagerAbilities.instance.absordDamage -= BulletBossScript.instance.damageBossBullet;
            }
            else
            {
                currentHP -= BulletBossScript.instance.damageBossBullet;
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurt.Play();
                }
                else
                {
                    hurt.Stop();
                }
                healthBar.SetHealth(currentHP);

            }

        }
        if (collision.gameObject.CompareTag("FBB"))
        {
            if (ManagerAbilities.instance.activeInvulnerability == true)
            {
                ManagerAbilities.instance.absordDamage -= FourthBossBullet.instance.damageBossBullet;
            }
            else
            {
                currentHP -= FourthBossBullet.instance.damageBossBullet;
                healthBar.SetHealth(currentHP);
                if (MenuInGame.instance.pauseActive == false)
                {
                    hurt.Play();
                }
                else
                {
                    hurt.Stop();
                }
                hurt.Play();
            }
        }
        if (collision.gameObject.CompareTag("Ammo"))
        {
            audioSource.PlayOneShot(ammoFill);
        }
    }
}
