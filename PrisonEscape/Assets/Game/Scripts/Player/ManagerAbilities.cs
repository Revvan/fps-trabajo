﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerAbilities : MonoBehaviour
{
    #region Singleton

    public static ManagerAbilities instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public GameObject rocket, canon, effect;
    public float shootForce;
    public int regenerationValue, rocketDamage, absordDamage;
    public float rocketBossDamage;
    public float timeToRegeneration, timeToShootRocket, timeWithInvulnerability, invulnerabilityCD;
    public bool activeFirstAbility, activeSecondAbility, activeThirdAbility, activeFourthAbility;
    public bool activeCD, activeInvulnerability, activeCDI, activeLastAbility;
    void Start()
    {
        
    }
    void Update()
    {
        ActivatedFirstAbility();

        if (!activeCD)
        {
            ActivatedSecondAbility();
        }
        else
        {
            timeToShootRocket -= Time.deltaTime;
            AbilitiesHUD.instance.cDSA.gameObject.SetActive(true);
            AbilitiesHUD.instance.cDSA.text = timeToShootRocket.ToString("0");

            if (timeToShootRocket <= 0)
            {
                timeToShootRocket = 6f;
                AbilitiesHUD.instance.cDSA.gameObject.SetActive(false);
                activeCD = false;
            }
        }

        ActiveThirdAbility();
        ActiveHiddenAbility();
    }
    private void ActivatedFirstAbility()
    {
        if (activeFirstAbility)
        {
            timeToRegeneration -= Time.deltaTime;
            AbilitiesHUD.instance.cDFA.gameObject.SetActive(true);
            AbilitiesHUD.instance.cDFA.text = timeToRegeneration.ToString("0");

            if (timeToRegeneration <= 0)
            {
                if (PlayerControllerScript.instance.currentHP == 100)
                {
                    PlayerControllerScript.instance.currentHP += 0;
                }
                if (PlayerControllerScript.instance.currentHP <= 90)
                {
                    PlayerControllerScript.instance.currentHP += regenerationValue;
                }
                if (PlayerControllerScript.instance.currentHP > 90)
                {
                    int a = PlayerControllerScript.instance.maxHP - PlayerControllerScript.instance.currentHP;
                    PlayerControllerScript.instance.currentHP += a;
                }
                timeToRegeneration = 15f;
            }
        }
    }
    private void ActivatedSecondAbility()
    {
        if (activeSecondAbility)
        {
            SecondBoss.instance.text.SetActive(false);
            SecondBoss.instance.rocketW.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                GameObject currentRocket = Instantiate(rocket, this.canon.transform.position, this.canon.transform.rotation);
                Rigidbody rb = currentRocket.GetComponent<Rigidbody>();
                GameObject currentEffect = Instantiate(effect, this.canon.transform.position, this.canon.transform.rotation);
                Destroy(currentEffect, 0.2f);
                rb.AddForce(this.canon.transform.forward * shootForce, ForceMode.Impulse);
                activeCD = true;
            }
        }
    }
    private void ActiveThirdAbility()
    {
        if (activeThirdAbility && !activeCDI)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                activeInvulnerability = true;
            }
            ThirdBoss.instance.text.SetActive(false);
            ThirdBoss.instance.stunT.SetActive(false);
            ThirdBoss.instance.inPhaseText.SetActive(false);
        }

        if (activeInvulnerability)
        {
            timeWithInvulnerability -= Time.deltaTime;
            AbilitiesHUD.instance.cDTA.gameObject.SetActive(true);
            AbilitiesHUD.instance.cDTA.text = timeWithInvulnerability.ToString("0");


            if (timeWithInvulnerability <= 0)
            {
                timeWithInvulnerability = 5f;
                activeCDI = true;
                AbilitiesHUD.instance.cDTA.gameObject.SetActive(false);
                activeInvulnerability = false;
            }
        }

        if (activeCDI)
        {
            invulnerabilityCD -= Time.deltaTime;
            AbilitiesHUD.instance.cDTA.gameObject.SetActive(true);
            AbilitiesHUD.instance.cDTA.text = invulnerabilityCD.ToString("0");

            if (invulnerabilityCD <= 0)
            {
                invulnerabilityCD = 20f;
                AbilitiesHUD.instance.cDTA.gameObject.SetActive(false);
                activeCDI = false;
            }
        }
    }
    private void ActiveHiddenAbility()
    {
        if (activeFourthAbility)
        {
            FourthBoss.instance.toClose.SetActive(false);
            FourthBoss.instance.text.SetActive(false);
            FourthBoss.instance.fPText.SetActive(false);
            FourthBoss.instance.sPText.SetActive(false);
            FourthBoss.instance.tPText.SetActive(false);
            FourthBoss.instance.rText.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                AbilitiesHUD.instance.hidden.SetActive(true);
                activeLastAbility = true;
            }
            AbilitiesHUD.instance.time -= Time.deltaTime;

            if (AbilitiesHUD.instance.time <= 0)
            {
                AbilitiesHUD.instance.hidden.SetActive(false);
                AbilitiesHUD.instance.time = 1f;
            }
        }
    }
}
